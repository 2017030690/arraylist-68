package com.example.agendaarraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> filtro;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        index = (int) bundleObject.getInt("index");
        filtro = contactos;

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("listaContactos", filtro);
                bundle.putInt("index", index);
                bundle.putBoolean("nuevo", true);
                intent.putExtras(bundle);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        cargarContactos();
    } //Fin onCreate

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void buscar(String s){
        ArrayList<Contacto> listaBuscar = new ArrayList<>();
        for (int x=0;x<filtro.size(); x++){
            if(filtro.get(x).getNombre().toUpperCase().contains(s.toUpperCase()))
                listaBuscar.add(filtro.get(x));
        }
        contactos = listaBuscar;
        tblLista.removeAllViews();
        cargarContactos();
    }

    public void cargarContactos() {
        for (int x=0; x < contactos.size(); x++) {
            final Contacto c = contactos.get(x);
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button btnVer = new Button(ListActivity.this);
            btnVer.setText(R.string.accver);
            btnVer.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            btnVer.setTextColor(Color.BLACK);
            Button btnBorrar = new Button(ListActivity.this);
            btnBorrar.setText(R.string.accborrar);
            btnBorrar.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            btnBorrar.setTextColor(Color.BLACK);

            btnVer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", (Contacto) v.getTag(R.string.contacto_g));
                    oBundle.putSerializable("listaContactos", filtro);
                    oBundle.putInt("index", (int) c.getId());
                    oBundle.putBoolean("nuevo", false);
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int y = 0; y < filtro.size(); y++)
                    {
                        if(filtro.get(y).getId() == (int) c.getId())
                        {
                            filtro.remove(filtro.get(y));
                            contactos = filtro;
                            break;
                        }
                    }
                    tblLista.removeAllViews();
                    cargarContactos();
                }
            });

            btnVer.setTag(R.string.contacto_g, c);
            btnVer.setTag(R.string.index, c.getId());
            btnBorrar.setTag(R.string.contacto_g, c);
            btnBorrar.setTag(R.string.index, c.getId());

            nRow.addView(btnVer);
            nRow.addView(btnBorrar);
            tblLista.addView(nRow);

        }
    }

}
